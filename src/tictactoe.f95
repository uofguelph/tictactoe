	PROGRAM TICTACTOE
		CHARACTER * 1 TICTAC (3,3)
		LOGICAL OVER
		CHARACTER WINNER
		INTEGER MOVE
		INTEGER TURN

		DATA TICTAC / " ", " ", " ", " ", " ", " ", " ", " ", " "/

		WRITE(*,*) "Welcome to Game of Tic Tac Toe"
		WRITE(*,*) "To play Enter any number between 1-9"
		WRITE(*,*) " "
		WRITE(*,*) "    1 	|    2     |   3    "
		WRITE(*,*) "  ------+----------+------- "
		WRITE(*,*) "    4   |    5    |    6    "
		WRITE(*,*) "  ------+----------+------- "
		WRITE(*,*) "    7   |    8    |    9    "

		CALL PLAYTICTACTOE(TICTAC,MOVE,WINNER,OVER,TURN)	

	END

!**********************************************************************
!THIS FUNCTION DOES EVERTHING IT PLAYS THE GAME OF TICTACTOE 
!CHECKS IF THE MOVE IS VALID, IF THE MOVE PLAYER CHOSE IS OCCUPIED 
!OR NOT AND CHECKS IF THE GAME IS OVER AND WHO WINS THE GAME (IF ANY)
!OR IF THE GAME IS A DRAW 

	SUBROUTINE PLAYTICTACTOE (TICTAC,MOVE,WINNER,OVER,TURN)
		CHARACTER * 1 TICTAC(3,3)
		INTEGER MOVE, TURN
		LOGICAL OVER,CHKPLAY
		CHARACTER WINNER

		DO
			DO
				!GET THE PLAYERS MOVE
				CALL GETMOVE  (MOVE)
				IF (MOVE > 0 .AND. MOVE < 10) THEN	
					!CHKPLAY THE PLAY IS OCCUPIED OR NOT
					IF (CHKPLAY(TICTAC,MOVE))THEN

						IF (MOVE == 1) THEN
							TICTAC(1,1) = "X"
							EXIT
						ELSE IF (MOVE == 2) THEN
							TICTAC(1,2) = "X"
							EXIT
						ELSE IF (MOVE == 3) THEN
							TICTAC(1,3) = "X"
							EXIT
						ELSE IF (MOVE == 4) THEN
							TICTAC(2,1) = "X"
							EXIT
						ELSE IF (MOVE == 5) THEN
							TICTAC(2,2) = "X"
							EXIT
						ELSE IF (MOVE == 6) THEN
							TICTAC(2,3) = "X"
							EXIT
						ELSE IF (MOVE == 7) THEN
							TICTAC(3,1) = "X"
							EXIT
						ELSE IF (MOVE == 8) THEN
							TICTAC(3,2) = "X"
							EXIT
						ELSE IF (MOVE == 9) THEN
							TICTAC(3,3) = "X"
							EXIT
						END IF 
					ELSE 
						WRITE(*,*) "THAT SPOT IS OCCUPID! CHOSE ANOTHER!"				
					END IF 			
				ELSE
					WRITE(*,*) "INVALID INPUT!!"
				END IF 
			END DO

			IF (TURN == 0) THEN
				WRITE(*,*) "AFTER YOUR MOVE..."
				TURN = 1
			END IF 	
			
			!PRINTS OUT THE BOARD
			CALL SHOWBOARD (TICTAC)
			CALL CHKOVR(TICTAC,OVER,WINNER)
				
			!CHECKS IF THE GAME IS OVER 
			IF (OVER)THEN
				IF (WINNER == "D")THEN
					WRITE(*,*) "ITS A DRAW"
					EXIT
				ELSE 
					WRITE(*,*) "WINNER IS: ",WINNER	
					EXIT
				END IF 
			END IF 
			
			!COMPUTERS TURN
			IF (TURN == 1) THEN 
				WRITE(*,*) "AFTER COMPUTERS MOVE..."
				CALL PICKMOVE(TICTAC)
				TURN = 0
			END IF
			CALL SHOWBOARD (TICTAC) 
			CALL CHKOVR(TICTAC,OVER,WINNER)
			
			IF (OVER)THEN
				IF (WINNER == "D")THEN
					WRITE(*,*) "ITS A DRAW"
					EXIT
				ELSE 
					WRITE(*,*) "WINNER IS: ",WINNER	
					EXIT
				END IF 
			END IF 		
		END DO
		 
	END 


!**********************************************************************
!this function draws the new board after human
! and computer move 	
	SUBROUTINE SHOWBOARD(TICTAC)
		CHARACTER *1 TICTAC(3,3)

		WRITE (*,*) TICTAC(1,1),"|",TICTAC(1,2),"|",TICTAC(1,3) 
		WRITE (*,*) "-+-+-"
		WRITE (*,*) TICTAC(2,1),"|",TICTAC(2,2),"|",TICTAC(2,3) 
		WRITE (*,*) "-+-+-"
		WRITE (*,*) TICTAC(3,1),"|",TICTAC(3,2),"|",TICTAC(3,3) 

	END 

! GET THE MOVE FROM THE USER AND CHECKS IF THE MOVE IS
! VAILD OR NOT 
	SUBROUTINE GETMOVE (MOVE)
		INTEGER MOVE

		WRITE(*,*) "PICK YOUR MOVE?"
		READ (*,*) MOVE
 		
	END

!**********************************************************************
! this function picks the move of a computer based on 
! the algorithm provided, first it try to  see if there a move to win 
! or it checks to block a playerfrom winning or else picks a random move 
	SUBROUTINE PICKMOVE(TICTAC)
	
		CHARACTER * 1 TICTAC(3,3)
		CHARACTER TIC1,TIC2,TIC3
	    INTEGER BOARD(9,2), K, X, Y, RANDPOS
	    DATA BOARD/1,1,1,2,2,2,3,3,3,1,2,3,1,2,3,1,2,3/
	    INTEGER T,P,D,C,SUMQ

	  	SUMQ = 0

	  	!checks if all each columns if there is a chance to win or block
	  	DO D=1,3
	  		DO C=1,3
	  			IF (TICTAC(D,C) == " ")THEN
		  			K = 0
		  		ELSE IF (TICTAC(D,C) == "X")THEN
		  			K = 1
		  		ELSE IF (TICTAC(D,C) == "O")THEN
		  			K = 4
		  		END IF 
		  		SUMQ  = SUMQ + K
		  	END DO
		  	IF (SUMQ == 8)THEN 
		  		IF (TICTAC(D,1) == " ")THEN
		  			TICTAC(D,1) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(D,2) == " ")THEN
		  			TICTAC(D,2) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(D,3) == " ")THEN
		  			TICTAC(D,3) = "O"
		  			RETURN
		  		END IF
		  	ELSE IF (SUMQ == 2)THEN
		  		IF (TICTAC(D,1) == " ")THEN
		  			TICTAC(D,1) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(D,2) == " ")THEN
		  			TICTAC(D,2) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(D,3) == " ")THEN
		  			RETURN
		  		END IF 				 
		  	END IF	
		  	SUMQ = 0  				
	  	END DO		
	  	
	  	!checks if all each rows if there is a chance to win or block
	  	DO P=1,3
	  		DO T=1,3
	  			IF (TICTAC(T,P) == " ")THEN
	  				K = 0	
	  			ELSE IF(TICTAC(T,P) == "X")THEN
	  				K = 1
	  			ELSE IF(TICTAC(T,P) == "O")THEN
	  				K = 4
	  			END IF
	  			SUMQ = SUMQ + K 
	  		END DO	

		  	IF (SUMQ == 8)THEN 
		  		IF (TICTAC(1,P) == " ")THEN
		  			TICTAC(1,P) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(2,P) == " ")THEN
		  			TICTAC(2,P) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(3,P) == " ")THEN
		  			TICTAC(3,P) = "O"
		  			RETURN
		  		END IF
		  	ELSE IF (SUMQ == 2)THEN
		  		IF (TICTAC(1,P) == " ")THEN
		  			TICTAC(1,P) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(2,P) == " ")THEN
		  			TICTAC(2,P) = "O"
		  			RETURN
		  		ELSE IF (TICTAC(3,P) == " ")THEN
		  			TICTAC(3,P) = "O"
		  			RETURN
		  		END IF 				 
		  	END IF
		  	SUMQ = 0
		END DO 

		SUMQ = 0
		TIC1 = TICTAC(1,1)
	  	TIC2 = TICTAC(2,2)
	  	TIC3 = TICTAC(3,3)

		!check the diagonal if there is a chance to win 
		IF (TIC1 == "O")THEN
			IF (TIC2== "O")THEN
				TICTAC(3,3) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC1== "O")THEN
			IF (TIC3 == "O")THEN
				TICTAC(2,2) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC2== "O")THEN
			IF (TIC3 == "O")THEN
				TICTAC(3,3) = "O"
				RETURN
			END IF
		END IF 

		IF (TIC1 == "X")THEN
			IF (TIC2== "X")THEN
				TICTAC(3,3) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC1== "X")THEN
			IF (TIC3 == "X")THEN
				TICTAC(2,2) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC2== "X")THEN
			IF (TIC3 == "X")THEN
				TICTAC(3,3) = "O"
				RETURN
			END IF
		END IF 

		TIC1 = TICTAC(1,3)
	  	TIC2 = TICTAC(2,2)
	  	TIC3 = TICTAC(3,1)
		
		IF (TIC1 == "O")THEN
			IF (TIC2== "O")THEN
				TICTAC(3,1) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC1== "O")THEN
			IF (TIC3 == "O")THEN
				TICTAC(2,2) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC2== "O")THEN
			IF (TIC3 == "O")THEN
				TICTAC(1,3) = "O"
				RETURN
			END IF
		END IF 

		IF (TIC1 == "X")THEN
			IF (TIC2== "X")THEN
				TICTAC(3,1) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC1== "X")THEN
			IF (TIC3 == "X")THEN
				TICTAC(2,2) = "O"
				RETURN
			END IF 
		END IF 
		IF (TIC2== "X")THEN
			IF (TIC3 == "X")THEN
				TICTAC(1,3) = "O"
				RETURN
			END IF
		END IF 

		DO 
	      		RANDPOS = INT(RAND(0)*9)+1
	      		X = BOARD(RANDPOS,1)
	      		Y = BOARD(RANDPOS,2)
	      		IF (TICTAC(X,Y) == " ") THEN
	        		TICTAC(X,Y) = "O"
	        		RETURN
	      		END IF  
		END DO    
		RETURN
	END

!**********************************************************************
! CHECKS IF THE COLUMS, ROWS, OR DIAGONAL ON THE BORAD ARE 
! THE SAME OR NOT
	LOGICAL FUNCTION SAME(TIC1,TIC2,TIC3)
		CHARACTER TIC1, TIC2, TIC3

		IF (TIC1 == "X" .AND. TIC2 == "X" .AND. TIC3 == "X") THEN
			SAME = .TRUE.
		ELSE IF (TIC1 == "O" .AND. TIC2 == "O" .AND. TIC3 == "O") THEN
			SAME = .TRUE.
		ELSE
			SAME = .FALSE.
		END IF 	
	END 

!**********************************************************************
! CHECK IF TIC-TAC-TOE IS OVER AND IF IT IS THEN
! IT DETERMINE THE WINNER 
! PARAMETER - BOARD , LOGICAL OVER AND WINNER OF THE GAME
	SUBROUTINE CHKOVR(TICTAC,OVER,WINNER)
		CHARACTER * 1 TICTAC(3,3)
		LOGICAL OVER
		CHARACTER WINNER
		CHARACTER * 1 BLANK,DRAW
		PARAMETER (BLANK = ' ', DRAW = 'D')

		LOGICAL SAME
		LOGICAL DSAME
		INTEGER IR,IC

		OVER = .TRUE.

		DO IR=1,3
		IF(SAME(TICTAC(IR,1), TICTAC(IR,2), TICTAC(IR,3))) THEN
			WINNER = TICTAC(IR,1)
			RETURN
		END IF
		END DO

		DO IC=1,3
		IF(SAME(TICTAC(1,IC), TICTAC(2,IC), TICTAC(3,IC))) THEN
			WINNER = TICTAC(1,IC)
			RETURN
		END IF 
		END DO
		


		DSAME = SAME(TICTAC(1,1), TICTAC(2,2), TICTAC(3,3)) &
		   .OR. SAME(TICTAC(1,3), TICTAC(2,2), TICTAC(3,1))
		
		IF (DSAME) THEN
			WINNER = TICTAC(2,2)
			RETURN
		END IF 
		
		DO IR=1,3
			DO IC = 1,3
				IF(TICTAC(IR,IC) == " ") THEN
					OVER = .FALSE.
					RETURN
				END IF	
			END DO
		END DO

		WINNER = DRAW
		RETURN

	END

!**********************************************************************
!CHECKS IF THE BOX OF THE BOARD IS FREE OR NOT OF THE 
! PLAYER THAT HAS SLECTED AS THEIR MOVE 
	LOGICAL FUNCTION CHKPLAY (TICTAC,MOVE)
		CHARACTER * 1 TICTAC(3,3)
		INTEGER MOVE

		IF (TICTAC(1,1) == " " .AND. MOVE == 1) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(1,2) == " " .AND. MOVE == 2) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(1,3) == " " .AND. MOVE == 3) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(2,1) == " " .AND. MOVE == 4) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(2,2) == " " .AND. MOVE == 5) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(2,3) == " " .AND. MOVE == 6) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(3,1) == " " .AND. MOVE == 7) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(3,2) == " " .AND. MOVE == 8) THEN
			CHKPLAY = .TRUE.
		ELSE IF (TICTAC(3,3) == " " .AND. MOVE == 9) THEN
			CHKPLAY = .TRUE.
		ELSE  
			CHKPLAY = .FALSE.
		END IF 
	END 